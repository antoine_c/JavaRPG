import java.util.Scanner;
import java.util.Random;

public class Combat 
{
    private PJ joueur1;
    private PJ joueur2;
	private String currentPlayer;
    
    public PJ getJoueur1() {
		return joueur1;
	}

	public void setJoueur1(PJ joueur1) {
		this.joueur1 = joueur1;
	}

	public PJ getJoueur2() {
		return joueur2;
	}

	public void setJoueur2(PJ joueur2) {
		this.joueur2 = joueur2;
	}


    public Combat(PJ joueur1, PJ joueur2) 
    {
        this.joueur1=joueur1;
        this.joueur2=joueur2;
    }

    public void setDegats(PJ joueur) 
    {
    	
    }
    
    public void levelUp() 
    {
    	
    }
    
    public void iniCombat()
    {
    	this.joueur1.setVIT(joueur1.getVIT());
    	this.joueur2.setVIT(joueur2.getVIT());
    	joueur1.ajoutInventaire();
    	joueur2.ajoutInventaire();
    	joueur1.setEXP(1);
    	joueur2.setEXP(1);
    }

    
    public int afficheMenuJeu()
    {
    	Scanner sc = new Scanner(System.in);
    	int reponseMenu=0;
    	System.out.println("MODE COMBAT ! [Tour du " +this.currentPlayer+ "] / Que voulez vous effectuer comme action ?");
		System.out.println("---------------------------------------------------------------------------------");
		while(reponseMenu<=0 || reponseMenu>4)
		{
			System.out.println("1 - Attaque ");
    		System.out.println("2 - Defense ");
    		System.out.println("3 - Soin ");
    		System.out.println("4 - Capitulation");
    		reponseMenu=sc.nextInt();
		}
		return reponseMenu;
    }
    
    public void menuJeu()
    {
    	int repMenu = 0; boolean curPlayerChanged=false;
    	double estDefendable = 0; boolean tourPrecedentAttaque=false; boolean winP1=false; boolean winP2=false;
    	int vieIniJ1=this.joueur1.getVIT();
    	int vieIniJ2=this.joueur2.getVIT();
    	this.currentPlayer="Joueur 1";
    	while(winP1==false && winP2==false)
    	{
    		if(this.joueur1.getVIT()<=0)
    		{
    			winP2=true;
    		}
    		else if(this.joueur2.getVIT()<=0)
    		{
    			winP1=true;
    		}
    		//this.debug("DEBUT WHILE VITALITE");
    		curPlayerChanged=false;
    		if(tourPrecedentAttaque==true)
    		{
    			System.out.println("Votre adversaire a lancé une attaque ! Vous pouvez essayer de vous defendre");
    		}
    		while(curPlayerChanged==false)
    		{
    			repMenu=afficheMenuJeu();
        		if(repMenu==1)
        		{
        			if(tourPrecedentAttaque==true)
        			{
        				int deg=(int)estDefendable;
        				this.attributionDegats(deg);
        				System.out.println("Pas de défense, vous prenez la totalité des degats (" + deg+")");
        			}
        			estDefendable=this.attaque(this.choixCap());
        			if(estDefendable>0)
        			{
        				tourPrecedentAttaque=true;
        				//this.debug("CALCUL ATTAQUE FAIT, PARABLE");
        			}
        		}
        		else if(repMenu==2)
        		{
        			if(tourPrecedentAttaque==false)
        			{
        				System.out.println("Aucune attaque n'a été lancée ... Votre tour passe");
        			}
        			else
        			{
        				//this.debug("LANCEMENT DEFENSE");
        				this.defense(estDefendable);
        				tourPrecedentAttaque=false;
        			}
        		}
        		else if(repMenu==3)
        		{
        			if(tourPrecedentAttaque==true)
        			{
        				int deg=(int)estDefendable;
        				this.attributionDegats(deg);
        				System.out.println("Pas de défense, vous prenez la totalité des degats (" + deg+")");
        			}
        			if(this.currentPlayer=="Joueur 1")
        			{
        				if(vieIniJ1>this.joueur1.getVIT())
        				{
        					//this.debug("SOINS POSSIBLE J1");
        					this.soin(vieIniJ1);
        				}
        				else
        				{
        					//this.debug("VIE JOUEUR 1 == "+this.joueur1.getVIT()+" | Vie max == " +vieIniJ1);
        					this.debug("SOINS IMPOSSIBLE J1");
        				}
        			}
        			else if(this.currentPlayer=="Joueur 2")
        			{
        				if(vieIniJ2>this.joueur2.getVIT())
        				{
        					//this.debug("SOINS POSSIBLE J2");
        					this.soin(vieIniJ2);
        				}
        				else
        				{
        					//this.debug("VIE JOUEUR 1 == "+this.joueur2.getVIT()+" | Vie max == " +vieIniJ2);
        					this.debug("SOINS IMPOSSIBLE J2");
        				}
        			}
        			tourPrecedentAttaque=false;
        		}
        		else if(repMenu==4)
        		{
        			if(this.currentPlayer=="Joueur 1")
        			{
        				System.out.println("Vous abandonnez :(");
        				winP2=true;
        			}
        			else if(this.currentPlayer=="Joueur 2")
        			{
        				System.out.println("Vous abandonnez :(");
        				winP1=true;
        			}
        		}
        		if(this.currentPlayer=="Joueur 1")
        		{
        			this.currentPlayer="Joueur 2";
        			System.out.println("Tour du Joueur 2");
        		}
        		else
        		{
        			this.currentPlayer="Joueur 1";
        			System.out.println("Tour du Joueur 1");
        		}
        		curPlayerChanged=true;
        		//this.debug("JOUEUR CHANGE");
				
    		}
    		if(this.joueur1.getVIT()<=0)
    		{
    			this.joueur1.setVIT(0);
    			winP2=true;
    		}
    		else if(this.joueur2.getVIT()<=0)
    		{
    			this.joueur2.setVIT(0);
    			winP1=true;
    		}
    		System.out.println("\nVie J1 -> " + this.joueur1.getVIT() + "\nVie J2 -> " + this.joueur2.getVIT());
    		System.out.println("\n");
    	}
    	if(winP2==true)
    	{
    		this.debug("Le joueur 2 a gagné !");
    		System.out.println("Votre expèrience augmente ..");
    		this.joueur2.setEXP(this.joueur2.getEXP()+1);
    		System.out.println("Vos caractéristiques augmentent ..");
    		this.joueur2.setFOR(this.joueur2.getFOR()+5);
    		this.joueur2.setCON(this.joueur2.getCON()+5);
    		this.joueur2.setDEX(this.joueur2.getDEX()+5);
    		this.joueur2.setINTEL(this.joueur2.getINTEL()+5);
    		this.joueur2.setVIT(vieIniJ2+5);
    		System.out.println("\n"+this.joueur2.toString());
    	}
    	else if(winP1==true)
    	{
    		this.debug("Le joueur 1 a gagné !");
    		System.out.println("Votre expèrience augmente ..");
    		this.joueur1.setEXP((this.joueur1.getEXP())+(1));
    		System.out.println("Vos caractéristiques augmentent ..");
    		this.joueur1.setFOR(this.joueur1.getFOR()+5);
    		this.joueur1.setCON(this.joueur1.getCON()+5);
    		this.joueur1.setDEX(this.joueur1.getDEX()+5);
    		this.joueur1.setINTEL(this.joueur1.getINTEL()+5);
    		this.joueur1.setVIT(vieIniJ1+5);
    		System.out.println("\n"+this.joueur1.toString());
    	}
    }
    
    public void soin(int vieMaxJoueur)
    {
    	Random r = new Random();
    	int random = r.nextInt(100);
    	int statPba=0; int statEff=0; double pba=0; double eff=0;
    	//this.debug("Generation Al�atoire : " + random);
    	if(this.currentPlayer=="Joueur 1")
    	{
    		if(this.joueur1.getRole()=='b')
    		{
    			statPba=this.joueur1.getDEX();
    			eff=this.joueur1.getFOR()/2+(this.joueur1.getInv().getS().getREST()/2);
    		}
    		else if(this.joueur1.getRole()=='c')
    		{
    			statPba=this.joueur1.getINTEL();
    	    	eff=this.joueur1.getCON()/2+(this.joueur1.getInv().getS().getREST()/2);
    		}
    		else
    		{
    			eff=this.joueur1.getDEX()/2+(this.joueur1.getInv().getS().getREST()/2);
    			statPba=this.joueur1.getINTEL();
    		}
    		pba=this.joueur1.getInv().getS().getCH()+(statPba/4);
    		//System.out.println("Pba : " + pba + "\nEff : " + eff);
    		this.debug("Tirage al�atoire : " +random);
    		if(random<pba)
    		{
    			System.out.println("Stat soins : " +this.joueur1.getInv().getS().getREST() );
    			System.out.println("Soin de " + eff + "point de vitalit�");
    			int effTemp = (int) eff;
    			if(this.joueur1.getVIT()+effTemp>vieMaxJoueur)
    			{
    				this.debug(this.joueur1.getVIT() + "+" + effTemp+ ">" + vieMaxJoueur);
    				this.joueur1.setVIT(vieMaxJoueur);
    			}
    			else
    			{
    				this.joueur1.setVIT(this.joueur1.getVIT()+effTemp);
    			}
    		}
    		else
    		{
    			System.out.println("Votre soin a échoué !");
    		}
    	}
    	if(this.currentPlayer=="Joueur 2")
    	{
    		if(this.joueur2.getRole()=='b')
    		{
    			statPba=this.joueur2.getDEX();
    			eff=this.joueur2.getFOR()/2+(this.joueur2.getInv().getS().getREST()/2);
    		}
    		else if(this.joueur2.getRole()=='c')
    		{
    			statPba=this.joueur2.getINTEL();
    	    	eff=this.joueur2.getCON()/2+(this.joueur2.getInv().getS().getREST()/2);
    		}
    		else
    		{
    			eff=this.joueur2.getDEX()/2+(this.joueur2.getInv().getS().getREST()/2);
    			statPba=this.joueur2.getINTEL();
    		}
    		pba=this.joueur2.getInv().getS().getCH()+(statPba/4);
    		//System.out.println("Pba : " + pba + "\nEff : " + eff);
    		if(random<pba)
    		{
    			System.out.println("Stat soins : " +this.joueur2.getInv().getS().getREST() );
    			System.out.println("Soin de " + eff + "point de vitalité");
    			int effTemp = (int) eff;
    			if(this.joueur2.getVIT()+effTemp>vieMaxJoueur)
    			{
    				this.debug(this.joueur2.getVIT() + "+" + effTemp+ ">" + vieMaxJoueur);
    				this.joueur2.setVIT(vieMaxJoueur);
    			}
    			else
    			{
    				this.joueur2.setVIT(this.joueur2.getVIT()+effTemp);
    			}
    		}
    		else
    		{
    			System.out.println("Votre soin a échoué !");
    		}
    	}
    }
    
    public String getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(String currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public void defense(double montantAttaque)
    {
    	Random r = new Random();
    	int random = r.nextInt(20);
    	int statPba=0;int statEff=0;double pba=0;double eff=0;
    	//this.debug("Generation al�atoire " + random);
    	if(this.currentPlayer=="Joueur 1")
    	{
    		if(joueur1.getRole()=='m')
    		{
    			statPba=this.joueur1.getINTEL();
    			statEff=this.joueur1.getCON();
    			System.out.println("Stat pba (Intel): "+statPba+"\nStat eff (Concentration): "+statEff);
    		}
    		else
    		{
    			statPba=this.joueur1.getDEX();
    			statEff=this.joueur1.getFOR();
    			System.out.println("Stat pba (Dextérité): "+statPba+"\nStat eff (Force): "+statEff);
    		}
    		pba=this.joueur1.getInv().getD().getPBA(statPba);
    		eff=this.joueur1.getInv().getD().getEFF(statEff);
    		//System.out.println("Pba : " + pba + "\nEff : " + eff);
    		this.debug("Tirage aléatoire : " +random);
    		if(random<pba)
    		{
    			System.out.println("Stat protection : " +this.joueur1.getInv().getD().getPRO() );
    			double pourcentageParee=(double)this.joueur1.getInv().getD().getPRO();
    			System.out.println("Attaque Parée !");
    			System.out.println("Montant parée : " +pourcentageParee + "%");
    			pourcentageParee=pourcentageParee/100;
    			montantAttaque=montantAttaque*(1-pourcentageParee);
    			System.out.println("Vous recevez seulement " + (int)montantAttaque + " points de degats");
    			int nouvelleVIT=this.joueur1.getVIT()-(int)montantAttaque;
    			this.joueur1.setVIT(nouvelleVIT);
    		}
    		else
    		{
    			//this.debug("FAIL DEFENSE");
    			System.out.println("Defense échouée !\n");
    			int montantFini=(int)montantAttaque;
    			this.attributionDegats(montantFini);
    		}
    	}
    	if(this.currentPlayer=="Joueur 2")
    	{
    		if(joueur1.getRole()=='m')
    		{
    			statPba=this.joueur2.getINTEL();
    			statEff=this.joueur2.getCON();
    			System.out.println("Stat pba (Intel): "+statPba+"\nStat eff (Concentration): "+statEff);
    		}
    		else
    		{
    			statPba=this.joueur2.getDEX();
    			statEff=this.joueur2.getFOR();
    			System.out.println("Stat pba (Dextérité): "+statPba+"\nStat eff (Force): "+statEff);
    		}
    		pba=this.joueur2.getInv().getD().getPBA(statPba);
    		eff=this.joueur2.getInv().getD().getEFF(statEff);
    		//System.out.println("Pba : " + pba + "\nEff : " + eff);
    		if(random<pba)
    		{
    			System.out.println("Stat protection : " +this.joueur2.getInv().getD().getPRO() );
    			double pourcentageParee=(double)this.joueur2.getInv().getD().getPRO();
    			System.out.println("Attaque Parée !");
    			System.out.println("Montant parée : " +pourcentageParee + "%");
    			pourcentageParee=pourcentageParee/100;
    			montantAttaque=montantAttaque*pourcentageParee;
    			System.out.println("Vous recevez seulement " + montantAttaque + " points de degats");
    			int nouvelleVIT=this.joueur2.getVIT()-(int)montantAttaque;
    			this.joueur2.setVIT(nouvelleVIT);
    		}
    		else
    		{
    			//this.debug("FAIL DEFENSE");
    			System.out.println("Défense échouée !\n");
    			int montantFini=(int)montantAttaque;
    			this.attributionDegats(montantFini);
    		}
    	}
    	//this.debug("FIN DE DEFENSE");
    }
    
	public void attributionDegats(int montantDegats) //Si aucune defense n'est faite
	{
		if(this.currentPlayer=="Joueur 1")
		{
			this.joueur1.setVIT(this.joueur1.getVIT()-montantDegats);
		}
		else if(this.currentPlayer=="Joueur 2")
		{
			this.joueur2.setVIT(this.joueur2.getVIT()-montantDegats);
		}
	}
	
    public int choixCap()
    {
    	int rep=0;
    	Scanner sc = new Scanner(System.in);
    	while(rep>6 || rep<1)
    	{
    		System.out.println("Choissisez votre capacité d'attaque :");
    		if(this.currentPlayer=="Joueur 1")
    		{
    			this.joueur1.getInv().getA().afficheCapacite();
    		}
    		else if(this.currentPlayer=="Joueur 2")
    		{
    			this.joueur2.getInv().getA().afficheCapacite();
    		}
    		rep=sc.nextInt();
    		
    	}
    	return rep;
    }
    
    public double attaque(int nbCap) //nbCap[1,6] et this.currentPlayer
    {
    	Random r = new Random();
		int random = r.nextInt(100);
		//System.out.println("G�n�ration al�atoire : " + random);
    	int statPba; double pba; int statEff; double eff; double degCap; double chCap; double chCumul; double degCumul=0;
    	if(this.currentPlayer=="Joueur 1")
    	{
    		if(joueur1.getRole()=='m')
    		{
    			statPba=this.joueur1.getINTEL();
    			statEff=this.joueur1.getCON();
    			System.out.println("Stat de Probabilité (Intel): "+statPba+"\nStat d'Efficacité (Concentration): "+statEff);
    			
    		}
    		else
    		{
    			statPba=this.joueur1.getDEX();
    			statEff=this.joueur1.getFOR();
    			System.out.println("Stat de Probabilité (Dexterite): "+statPba+"\nStat d'Efficacité (Force): "+statEff);
    		}
    		pba=this.joueur1.getInv().getA().getPBA(statPba);
			eff=this.joueur1.getInv().getA().getEFF(statEff);
    		//System.out.println("Pba : " + pba + "\nEff : " + eff);
    		chCap=this.joueur1.getInv().getA().getArc().get(nbCap-1).getChance();
    		degCap=this.joueur1.getInv().getA().getArc().get(nbCap-1).getDegats();
    		chCumul=(chCap*100)+(pba*10);
    		System.out.println("Capacitée choisie : "+this.joueur1.getInv().getA().getArc().get(nbCap-1).getNomCap());
    		//System.out.println("Chance : "+chCap+" | Degats : "+degCap);
    		System.out.println("Chance : "+chCumul);
    		this.debug("Tirage aléatoire : " +random);
    		if(random<chCumul)
    		{
    			System.out.println("Attaque réussie !["+random+"<"+chCumul+"]");
    			degCumul=degCap+eff;
    			System.out.println("\nDegats Cumulés (Capacité+Arme) :"+degCumul);
    		}
    		else
    		{
    			System.out.println("\nAttaque échouée !");
    			degCumul=0;
    		}
    	}
    		
    	else if(this.currentPlayer=="Joueur 2")
    	{
    		if(joueur1.getRole()=='m')
    		{
    			statPba=this.joueur2.getINTEL();
    			statEff=this.joueur2.getCON();
    			System.out.println("Stat de Probabilité (Intel): "+statPba+"\nStat d'Efficacité (Concentration): "+statEff);
    			
    		}
    		else
    		{
    			statPba=this.joueur2.getDEX();
    			statEff=this.joueur2.getFOR();
    			System.out.println("Stat de Probabilité (Dextérité): "+statPba+"\nStat d'Efficacité (Force): "+statEff);
    		}
    		pba=this.joueur2.getInv().getA().getPBA(statPba);
			eff=this.joueur2.getInv().getA().getEFF(statEff);
    		//System.out.println("Pba : " + pba + "\nEff : " + eff);
    		chCap=this.joueur2.getInv().getA().getArc().get(nbCap-1).getChance();
    		degCap=this.joueur2.getInv().getA().getArc().get(nbCap-1).getDegats();
    		chCumul=(chCap*100)+(pba*10);
    		System.out.println("Capacitée choisie : "+this.joueur2.getInv().getA().getArc().get(nbCap-1).getNomCap());
    		//System.out.println("Chance : "+chCap+" | Degats : "+degCap);
    		System.out.println("Chance : "+chCumul);
    		if(random<chCumul)
    		{
    			System.out.println("Attaque réussie !["+random+"<"+chCumul+"]");
    			degCumul=degCap+eff;
    			System.out.println("Degats Cumulé :"+degCumul);
    		}
    		else
    		{
    			System.out.println("\nAttaque échouée !");
    			degCumul=0;
    		}
    	}
		return degCumul;
    }
    
    public void debug(String msg)
    {
    	System.out.println("\n----------------------------------------------------------------");
    	System.out.println(msg);
    	System.out.println("----------------------------------------------------------------\n");
    }

}
