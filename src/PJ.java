/*
    Choisir un type de personnage (guerrier, chasseur ou magicien) et son nom
    Choisir la valeur de ses aptitudes 
    (force, dext�rit�, intelligence, concentration) en respectant les contraintes d�taill�es plus haut
    Lui affecter une exp�rience de 1
    Cr�er 2 capacit�s en respectant les contraintes expos�es plus haut, puis les lui affecter

*/
import java.util.Scanner;

public class PJ 
{
    private String nom;
    private int FOR;
    private int DEX;
    private int INTEL;
    private int CON;
    private int VIT; // La vitalit� d�un personnage est son nombre de points de vie.
    private int EXP;
    private char role;
    private Inventaire inv;

    public PJ() {
    }

    public PJ(String nom, int FOR, int DEX, int INTEL, int CON, int VIT, int EXP, char role, Inventaire inv) {
        this.nom=nom;
        this.FOR=FOR;
        this.DEX=DEX;
        this.INTEL=INTEL;
        this.CON=CON;
        this.VIT=VIT;
        this.EXP=EXP;
        this.role=role;
        this.inv=inv;
    }

    public PJ(PJ joueur1) {
        this.nom=joueur1.nom;
        this.DEX=joueur1.DEX;
        this.FOR=joueur1.FOR;
        this.INTEL=joueur1.INTEL;
        this.CON=joueur1.CON;
        this.VIT=joueur1.VIT;
        this.EXP=joueur1.EXP;
        this.role=joueur1.role;
        this.inv=joueur1.inv;
    }

    public String toString() {
        String c="Caract�ristiques joueur : "
        	+"\n-----------------------------------------\n"
        	+"Nom :"+this.nom
        	+"\n"+"Force :"+this.FOR+
        	"\n"+"Dext�rit� :"+this.DEX
        	+"\n"+"Intelligence :" +this.INTEL+"\n"
        	+"Concentration :" +this.CON+"\n"
        	+"Vitalit� :"+this.VIT
        	+"\n"+"Exp�rience :" +this.EXP+1
        	+"\n------------------------------------------\n";
        return c;
    }
    
    /*
	public void toStringVoid()
    {
    	String c[][] = new String
    		{{"------------------------"},
    		 {"-                      -"},
    		 {"-                      -"},
    		 {"-                      -"},
    		 {"-                      -"},
    		 {"-                      -"},
    		 {"-                      -"},
    		 {"-                      -"},
    		 {"------------------------"},};
    	for(int i=0;i<8;i++)
    	{
    		for(int j=0;j<24;j++)
    		{
    			if(i==1 && j==1)
    			{
    				c[i][j]="Nom :"+this.nom;
    			}
    		}
    	}
    	for(int i=0;i<24;i++)
    	{
    		for(int j=0;j<24;i++)
    		{
    			if(j==24)
    			{
    				System.out.print("\n");
    			}
    			System.out.print(c[i][j]);
    		}
    	}
    }*/

//GETTERS SETTERS
    public void setNom(String nom) {
        this.nom=nom;
    }

    public void setFOR(int FOR) {
        this.FOR=FOR;
    }

    public void setDEX(int DEX) {
        this.DEX=DEX;
    }

    public void setINTEL(int INTEL) {
        this.INTEL=INTEL;
    }

    public void setCON(int CON) {
        this.CON=CON;
    }

    public void setVIT(int VIT) {
        this.VIT=VIT;
    }

    public void setEXP(int EXP) {
        this.EXP=EXP;
    }

    public void setRole(char c) {
        this.role=c;
    }

    public String getNom() {
        return this.nom;
    }

    public int getDEX() {
        return this.DEX;
    }

    public int getFOR() {
        return this.FOR;
    }

    public int getINTEL() {
        return this.INTEL;
    }

    public int getVIT() {
        return this.VIT;
    }

    public int getEXP() {
        return this.EXP;
    }

    public int getCON() {
        return this.CON;
    }

    public char getRole() {
        return this.role;
    }

//Fonction d'affichage pour clarifier
    public void debug(String msg) {
        System.out.println("\n------------------------------------------------\n" + msg + "\n------------------------------------------------\n");
    }

//En fonction du role, affecte des propriétés
    public boolean iniStat(char role) {
        boolean verifSaisie=false;
        this.setVIT(100);
        if(role=='c')//FOR >= 20 && DEX >= 20 && INT >= 20 && CON >= 20
        {
            verifSaisie=true;
            this.setFOR(20);
            this.setDEX(20);
            this.setINTEL(20);
            this.setCON(20);
            //contrainteClasse="Chasseur de galinette cendr�e";
        }
        else if(role=='m')//INT >= max(FOR, DEX) + 15 && CON >= max(FOR, DEX) + 15
        {
            this.setINTEL(15);
            this.setCON(15);
            this.setFOR(0);
            this.setDEX(0);
            verifSaisie=true;
            //contrainteClasse="Mage !\nIntelligence et Concentration > Force et Dext�rit�";
        
        }
        else if(role=='b') //FOR >= (DEX + 10) >= (INT + 10) >= CON
        {
            this.setFOR(0);
            this.setDEX(0);
            this.setINTEL(0);
            this.setCON(0);
            verifSaisie=true;
            //contrainteClasse="Barbare !\nForce > Dext�rit� + 10 > Intelligence +10 > Concentration";
        }
        return verifSaisie;
    }

    public void saisieRole() {
        Scanner sc1 = new Scanner(System.in);
        //boolean verifSaisie=false;
        boolean verifBoucle=false;
        while(verifBoucle==false)
        {
            System.out.println("Vous pouvez choisir : Chasseur(c), Mage(m) ou Brute(b)");
            System.out.println("Saisissez votre classe : ");
            char role = sc1.next().charAt(0); //Saisie
            //this.debug("Saisie FAITE");
            if(this.iniStat(role)!=false)
            {
                verifBoucle=true;
                this.setRole(role);
            }
        }
        /* 
        Boucle de saisie : 
        --------------------------------------------------------------------------
        Vous avez choisi *this.getClasse()* , vos capacit�s de base sont :
        FOR : 
        DEX : 
        INT :
        
        */
    }

    public boolean attributionValide(int totalCompetences) {
        boolean result = false;
        System.out.println(this.toString());
        if(totalCompetences==0)
        {
           System.out.println("Vos comp�tences sont valides !"); // totalcompetences envoyé est bien égal à 0
            //FOR >= 20 && DEX >= 20 && INT >= 20 && CON >= 20
            if(this.getRole()=='c' 
                    && this.getFOR()>=20 
                    && this.getCON()>=20 
                    && this.getDEX()>=20 
                    && this.getINTEL()>=20
                    )
            {
                //this.debug("Chasseur VALIDE");                
                result=true;
            }
            //INT >= max(FOR, DEX) + 15 && CON >= max(FOR, DEX) + 15
            else if(role=='m' 
                    && this.getINTEL()>=this.getFOR()+15 
                    && this.getINTEL()>=this.getDEX()+15
                    && this.getCON()>=this.getFOR()+15
                    && this.getCON()>=this.getDEX()+15)
            {
                //this.debug("Mage VALIDE");
                result=true;
            }
            //FOR >= (DEX + 10) >= (INT + 10) >= CON
            else if(role=='b'
                    && this.getFOR()>=this.getDEX()+10
                    && this.getDEX()+10>=this.getINTEL()+10
                    && this.getINTEL()+10>=this.getCON())
            {
                //this.debug("Barbare VALIDE");
                result=true;
            }
        }
        return result;
    }

    public boolean attributionComp(char comp, int nbAjout, int totalRestant) {
        boolean b=false;
        if(totalRestant>0)
        {
            if(comp=='F')
            {
                this.setFOR(this.getFOR()+nbAjout);
                b=true;
            }
            if(comp=='D')
            {
                this.setDEX(this.getDEX()+nbAjout);
                b=true;
            }
            if(comp=='C')
            {
                this.setCON(this.getCON()+nbAjout);
                b=true;
            }
            if(comp=='V')
            {
                this.setVIT(this.getVIT()+nbAjout);
                b=true;
            }
            if(comp=='I')
            {
                this.setINTEL(this.getINTEL()+nbAjout);
                b=true;
            }
        }
        else
        {
            b=false;
        }
        return b;
    }
    
    public void afficheInv()
    {
    	System.out.println(this.inv.toString());
    }
    
    public void afficheArme()
    {
    	this.inv.afficheArme();
    }
    
    public Inventaire getInv() {
		return inv;
	}

	public void setInv(Inventaire inv) {
		this.inv = inv;
	}

	public void afficheDefense()
    {
    	this.inv.afficheDefense();
    }
    
    public void afficheSoin()
    {
    	this.inv.afficheSoin();
    }
    
    public void saisieNom()
    {
    	Scanner sc = new Scanner(System.in);
    	System.out.println("\nChoisissez vous un nom d'aventurier : ");
    	String name=sc.nextLine();
    	this.setNom(name);
    }

    public void iniRole() {
        this.saisieRole();
        this.saisieNom();
        boolean sortieWhile=false;
        this.iniStat(this.role);
        System.out.println(this);
        Scanner sc = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        boolean sortieBoucle=false;
        char saisieComp;
        int saisieNbAjout=0;
        int totalRestant=100-(this.getFOR()+this.getDEX()+this.getINTEL()+this.getCON());
        while(sortieWhile!=true)
        {
            if(totalRestant!=0)
            {
                System.out.println("Points restants � attriber : " + totalRestant);
                System.out.println("Choisissez la competences a augmenter : (F/D/I/C/V)");
                saisieComp=sc.next().charAt(0);
                System.out.println("De combien ? ");
                saisieNbAjout=sc2.nextInt();
                if(this.attributionComp(saisieComp,saisieNbAjout,totalRestant)==true)
                {
                    totalRestant=totalRestant-saisieNbAjout;
                    sortieWhile=this.attributionValide(totalRestant);
                }
            }
            else if(totalRestant==0)
            {
                sortieWhile=true;
            }
            
        }
    }
    
    public void ajoutInventaire()
    {
    	this.inv=new Inventaire();
    	this.inv.iniInventaire(this.role);
    }

}
/*
-Nom
-Force Physique FOR
-Dext�rit� DEX
-intelligence INTEL
-capacit� de concentration CON
-vitalit� VIT
-exp�rience EXP
-role : chasseur / magicien / barbare


JOUTER SAC QUAND CLASSE OP
*/
