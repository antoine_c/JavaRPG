import java.util.Random;

public class Defense 
{
	private int PRO;
	private int MAN;
	private char role;
	private String nom;
	
	public Defense()
	{
		
	}
	
	public Defense(int PRO,int MAN,char role,String nom)
	{
		this.PRO=PRO;
		this.MAN=MAN;
		this.role=role;
		this.nom=nom;
	}
	
	public int getPRO()
	{
		return this.PRO;
	}
	public int getMAN()
	{
		return this.MAN;
	}
	public char getRole()
	{
		return this.role;
	}
	public String getNom()
	{
		return this.nom;
	}
	public void setPRO(int PRO)
	{
		this.PRO=PRO;
	}
	public void setMAN(int MAN)
	{
		this.MAN=MAN;
	}
	public void setRole(char role)
	{
		this.role=role;
	}
	public void setNom(String nom)
	{
		this.nom=nom;
	}
	
	public void creerDefense(char role)
	{
		Random random0 = new Random();
		int random = random0.nextInt(3);
		if(role=='c')
		{
			if(random==0)
			{
				this.setPRO(40);
				this.setMAN(60);
				this.setNom("Roulade");
			}
			else if(random==1)
			{
				this.setPRO(80);
				this.setMAN(20);
				this.setNom("Esquive");
			}
			else if(random==2)
			{
				this.setPRO(20);
				this.setMAN(80);
				this.setNom("Toile magique");
			}
		}
		else if(role=='m')
		{
			if(random==0)
			{
				this.setPRO(50);
				this.setMAN(50);
				this.setNom("Peau de Pierre");
			}
			else if(random==1)
			{
				this.setPRO(80);
				this.setMAN(20);
				this.setNom("Rune Defensive");
			}
			else if(random==2)
			{
				this.setPRO(20);
				this.setMAN(80);
				this.setNom("Amulette Gardienne");
			}
		}
		else if(role=='b')
		{
			if(random==0)
			{
				this.setPRO(40);
				this.setMAN(60);
				this.setNom("Bouclier");
			}
			else if(random==1)
			{
				this.setPRO(80);
				this.setMAN(20);
				this.setNom("Armure Epineuse");
			}
			else if(random==2)
			{
				this.setPRO(20);
				this.setMAN(80);
				this.setNom("Ferveur");
			}
		}
		System.out.println("Génération de Defense [" + this.getNom() + "] ( Protection : " + this.getPRO() + " || Maniabilité : " + this.getMAN() + " )");
	}
	
	public float getPBA(int statPba)//dex * man /1000 OU int * man /1000
	{
		float pba=0;
		pba=(statPba*this.MAN)/100;
		//System.out.println("Pba : statPba("+statPba+") * Man("+this.getMAN()+")/100 ="+pba);
		return pba;
	}
	
	public float getEFF(int statEff)//for * pro /100 OU con * pro /100
	{
		float eff=0;
		eff=(statEff*this.PRO)/100;
		//System.out.println("Pba : statPba("+statEff+") * Pro("+this.getPRO()+")/100 ="+eff);
		return eff;
	}

	public String toString() 
	{
		return this.getNom() + "(Protection : " + this.getPRO() + " || Maniabilité : " + this.getMAN() + ")";
	}
}
