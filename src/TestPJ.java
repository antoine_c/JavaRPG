import java.util.Random;

public class TestPJ {

	public static void main(String[] args){
		Inventaire i1 = new Inventaire();
		Inventaire i2 = new Inventaire();
		PJ joueur3 = new PJ("j1",25,25,25,25,100,1,'b',i1); //defaut
		PJ joueur4 = new PJ("j2",25,25,25,25,100,1,'b',i2);
		
		//CREATION DES PERSONNAGES

		PJ joueur1 = new PJ();
		PJ joueur2 = new PJ();
		System.out.println("\n\nCréation du Joueur 1");
		joueur1.iniRole();
		System.out.println("\n\nCréation du Joueur 2");
		joueur2.iniRole();
		

		// LANCEMENT D'UN COMBAT
		Combat c = new Combat(joueur1,joueur2);
		c.iniCombat();
		c.menuJeu();
	}

}