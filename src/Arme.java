import java.util.ArrayList;
import java.util.Random;

public class Arme 
{
	private int PUI;
	private int FAC;
	private char role;
	private String nom;
	private ArrayList<Capacite> arc;
	
	public Arme()
	{
		
	}
	
	public Arme(int PUI,int FAC,char role,String nom, ArrayList<Capacite> arc)
	{
		this.PUI=PUI;
		this.FAC=FAC;
		this.role=role;
		this.nom=nom;
		this.arc=arc;
	}
	
	public void setPUI(int PUI)
	{
		this.PUI=PUI;
	}
	
	public void setFAC(int FAC)
	{
		this.FAC=FAC;
	}
	
	public void setRole(char role)
	{
		this.role=role;
	}
	
	public void setNom(String nom)
	{
		this.nom=nom;
	}
	
	public int getPUI()
	{
		return this.PUI;
	}
	
	public int getFAC()
	{
		return this.FAC;
	}
	
	public String getNom()
	{
		return this.nom;
	}
	
	public ArrayList<Capacite> getArc() {
		return arc;
	}

	public void setArc(ArrayList<Capacite> arc) {
		this.arc = arc;
	}

	public void creerArme(char role)
	{
		Random random0 = new Random();
		int random = random0.nextInt(3);
		if(role=='c')
		{
		this.role='c';
			if(random==0)
			{
				this.setNom("Tromblon");
				this.setPUI(80);
				this.setFAC(20);
			}
			else if(random==1)
			{
				this.setNom("Lance-pierre");
				this.setPUI(5);
				this.setFAC(95);
			}
			else if(random==2)
			{
				this.setNom("Arc Courb�");
				this.setPUI(50);
				this.setFAC(50);
			}
		}
		else if(role=='b')
		{
		this.role='b';
			if(random==0)
			{
				this.setNom("Hache");
				this.setPUI(80);
				this.setFAC(20);
			}
			else if(random==1)
			{
				this.setNom("Bout de bois");
				this.setPUI(5);
				this.setFAC(95);
			}
			else if(random==2)
			{
				this.setNom("Ep�e des rois");
				this.setPUI(50);
				this.setFAC(50);
			}
		}
		else if(role=='m')
		{
		this.role='m';
			if(random==0)
			{
				this.setNom("Sceptre des Anciens");
				this.setPUI(80);
				this.setFAC(20);
			}
			else if(random==1)
			{
				this.setNom("Baguette Magique");
				this.setPUI(5);
				this.setFAC(95);
			}
			else if(random==2)
			{
				this.setNom("Ghostbuster Gun");
				this.setPUI(50);
				this.setFAC(50);
			}
		}
		this.ajoutCapaciteArme();
		System.out.println("G�n�ration de l'Arme [" + this.getNom() + "] ( Puissance : " + this.getPUI() + " || Facilit� : " + this.getFAC() + " )");
	}
	
	public void ajoutCapaciteArmeSafe()
	{
		this.arc=new ArrayList<Capacite>();
		if(this.role=='c')
		{
			if(this.nom=="Tromblon")
			{
				Capacite c1=new Capacite("Tir de barrage",5,0,0,1);
				Capacite c2=new Capacite("Dechiqueter",10,0,0,0.9);
				Capacite c3=new Capacite("Tir de Pr�cision",15,0,0,0.8);
				Capacite c4=new Capacite("Tir � bout portant",20,0,0,0.7);
				Capacite c5=new Capacite("Vis�e",25,0,0,0.6);
				Capacite c6=new Capacite("Tir mortel",30,0,0,0.5);
				this.arc.add(c1);
				this.arc.add(c2);
				this.arc.add(c3);
				this.arc.add(c4);
				this.arc.add(c5);
				this.arc.add(c6);
			}
			else if(this.nom=="Lance-pierre")
			{
				Capacite c1=new Capacite("Tir � vide",5,0,0,1);
				Capacite c2=new Capacite("Lanc� de cailloux",10,0,0,0.9);
				Capacite c3=new Capacite("Parachutage d'�cureuil",15,0,0,0.8);
				Capacite c4=new Capacite("Tir � bout portant",20,0,0,0.7);
				Capacite c5=new Capacite("Vis�e",25,0,0,0.6);
				Capacite c6=new Capacite("Lanc� de dynamite",30,0,0,0.5);
				this.arc.add(c1);
				this.arc.add(c2);
				this.arc.add(c3);
				this.arc.add(c4);
				this.arc.add(c5);
				this.arc.add(c6);
			}
			else if(this.nom=="Arc Courb�")
			{
				Capacite c1=new Capacite("Tir de fl�che en bois",5,0,0,1);
				Capacite c2=new Capacite("Lanc� d'arme",10,0,0,0.9);
				Capacite c3=new Capacite("Parachutage d'�cureuil",15,0,0,0.8);
				Capacite c4=new Capacite("Tir � bout portant",20,0,0,0.7);
				Capacite c5=new Capacite("Vis�e",25,0,0,0.6);
				Capacite c6=new Capacite("Tir mortel",30,0,0,0.5);
				this.arc.add(c1);
				this.arc.add(c2);
				this.arc.add(c3);
				this.arc.add(c4);
				this.arc.add(c5);
				this.arc.add(c6);
			}
		}
		else if(this.role=='m')
		{
			if(this.nom=="Sceptre des Anciens")
			{
				Capacite c1=new Capacite("Etincelle",5,0,0,1);
				Capacite c2=new Capacite("Molotov",10,0,0,0.9);
				Capacite c3=new Capacite("Brasier",15,0,0,0.8);
				Capacite c4=new Capacite("Embrasement",20,0,0,0.7);
				Capacite c5=new Capacite("Boule de feu",25,0,0,0.6);
				Capacite c6=new Capacite("Col�re des dieux",30,0,0,0.5);
				this.arc.add(c1);
				this.arc.add(c2);
				this.arc.add(c3);
				this.arc.add(c4);
				this.arc.add(c5);
				this.arc.add(c6);
			}
			else if(this.nom=="Baguette Magique")
			{
				Capacite c1=new Capacite("Stupefix",5,0,0,1);
				Capacite c2=new Capacite("Petrificus Totalus",10,0,0,0.9);
				Capacite c3=new Capacite("Malus",15,0,0,0.8);
				Capacite c4=new Capacite("Motux et bouche cousux",20,0,0,0.7);
				Capacite c5=new Capacite("Coup du chapeau",25,0,0,0.6);
				Capacite c6=new Capacite("Avada Kedavra",30,0,0,0.5);
				this.arc.add(c1);
				this.arc.add(c2);
				this.arc.add(c3);
				this.arc.add(c4);
				this.arc.add(c5);
				this.arc.add(c6);
			}
			else if(this.nom=="Ghostbuster Gun")
			{
				Capacite c1=new Capacite("Capture d'�me",5,0,0,1);
				Capacite c2=new Capacite("Jet de miasma",10,0,0,0.9);
				Capacite c3=new Capacite("Projection Astrale",15,0,0,0.8);
				Capacite c4=new Capacite("Fantome d�sanusseur de veuve",20,0,0,0.7);
				Capacite c5=new Capacite("Invocarion d'Adolph",25,0,0,0.6);
				Capacite c6=new Capacite("Invocation Casper",30,0,0,0.5);
				this.arc.add(c1);
				this.arc.add(c2);
				this.arc.add(c3);
				this.arc.add(c4);
				this.arc.add(c5);
				this.arc.add(c6);
			}
		}
		else if(this.role=='b')
		{
			if(this.nom=="Hache")
			{
				Capacite c1=new Capacite("Coup de manche",5,0,0,1);
				Capacite c2=new Capacite("Tranche t�te",10,0,0,0.9);
				Capacite c3=new Capacite("Technique du bucheron",15,0,0,0.8);
				Capacite c4=new Capacite("Attaque de nain",20,0,0,0.7);
				Capacite c5=new Capacite("Lanc� de hache",25,0,0,0.6);
				Capacite c6=new Capacite("Toupie sanglante",30,0,0,0.5);
				this.arc.add(c1);
				this.arc.add(c2);
				this.arc.add(c3);
				this.arc.add(c4);
				this.arc.add(c5);
				this.arc.add(c6);
			}
			else if(this.nom=="Bout de bois")
			{
				Capacite c1=new Capacite("Pichenette",5,0,0,1);
				Capacite c2=new Capacite("Coup de batte",10,0,0,0.9);
				Capacite c3=new Capacite("Charpente extr�me",15,0,0,0.8);
				Capacite c4=new Capacite("Technique du menuisier",20,0,0,0.7);
				Capacite c5=new Capacite("Coup aux testicules",25,0,0,0.6);
				Capacite c6=new Capacite("Lanc� de poutre",30,0,0,0.5);
				this.arc.add(c1);
				this.arc.add(c2);
				this.arc.add(c3);
				this.arc.add(c4);
				this.arc.add(c5);
				this.arc.add(c6);
			}
			else if(this.nom=="Ep�e des rois")
			{
				Capacite c1=new Capacite("Estoc",5,0,0,1);
				Capacite c2=new Capacite("Taille",10,0,0,0.9);
				Capacite c3=new Capacite("Tourbillon",15,0,0,0.8);
				Capacite c4=new Capacite("Tr�panation",20,0,0,0.7);
				Capacite c5=new Capacite("Eventrement",25,0,0,0.6);
				Capacite c6=new Capacite("D�capitation",30,0,0,0.5);
				this.arc.add(c1);
				this.arc.add(c2);
				this.arc.add(c3);
				this.arc.add(c4);
				this.arc.add(c5);
				this.arc.add(c6);
			}
		}
	}
	
	public void ajoutCapaciteArme()
	{
		int deg[]={0,0,0,0,0,0};
		int soin[]={0,0,0,0,0,0};
		int blocage[]={0,0,0,0,0,0};
		double chance[]={0,0,0,0,0,0};
		String nom[][][][][][]= null;
		this.arc=new ArrayList<Capacite>();
		deg[0]=5; chance[0]=1.0;
		deg[1]=10; chance[1]=0.9;
		deg[2]=15; chance[2]=0.8;
		deg[3]=20; chance[3]=0.7;
		deg[4]=25; chance[4]=0.6;
		deg[5]=30; chance[5]=0.5;
		Capacite c1=new Capacite("",deg[0],soin[0],blocage[0],chance[0]);
		Capacite c2=new Capacite("",deg[1],soin[1],blocage[1],chance[1]);
		Capacite c3=new Capacite("",deg[2],soin[2],blocage[2],chance[2]);
		Capacite c4=new Capacite("",deg[3],soin[3],blocage[3],chance[3]);
		Capacite c5=new Capacite("",deg[4],soin[4],blocage[4],chance[4]);
		Capacite c6=new Capacite("",deg[5],soin[5],blocage[5],chance[5]);
		if(this.role=='c')
		{
			if(this.nom=="Tromblon")
			{
				c1.setNomCap("Tir de barrage");
				c2.setNomCap("Dechiqueter");
				c3.setNomCap("Tir de Pr�cision");
				c4.setNomCap("Tir � bout portant");
				c5.setNomCap("Vis�e");
				c6.setNomCap("Tir mortel");
			}
			else if(this.nom=="Lance-pierre")
			{
				c1.setNomCap("Tir � vide");
				c2.setNomCap("Lanc� de cailloux");
				c3.setNomCap("Parachutage d'�cureuil");
				c4.setNomCap("Tir � bout portant");
				c5.setNomCap("Vis�e");
				c6.setNomCap("Lanc� de dynamite");
				
			}
			else if(this.nom=="Arc Courb�")
			{
				c1.setNomCap("Tir de fl�che en bois");
				c2.setNomCap("Lanc� d'arme");
				c3.setNomCap("Parachutage d'�cureuil");
				c4.setNomCap("Tir � bout portant");
				c5.setNomCap("Vis�e");
				c6.setNomCap("Tir mortel");
			}
		}
		else if(this.role=='m')
		{
			if(this.nom=="Sceptre des Anciens")
			{
				c1.setNomCap("Etincelle");
				c2.setNomCap("Molotov");
				c3.setNomCap("Brasier");
				c4.setNomCap("Embrasement");
				c5.setNomCap("Boule de feu");
				c6.setNomCap("Col�re des dieux");
			}
			else if(this.nom=="Baguette Magique")
			{
				c1.setNomCap("Stupefix");
				c2.setNomCap("Petrificus Totalus");
				c3.setNomCap("Malus");
				c4.setNomCap("Motux et bouche Cousux");
				c5.setNomCap("Coup du Chapeau");
				c6.setNomCap("Avada Kedavra");
			}
			else if(this.nom=="Ghostbuster Gun")
			{
				c1.setNomCap("Capture d'�me");
				c2.setNomCap("Jet de Miasma");
				c3.setNomCap("Projection Astrale");
				c4.setNomCap("Fantome");
				c5.setNomCap("Invocation de D�mon");
				c6.setNomCap("Invocation de Casper");
			}
		}
		else if(this.role=='b')
		{
			if(this.nom=="Hache")
			{
				c1.setNomCap("Coup de Manche");
				c2.setNomCap("Tantre Tete");
				c3.setNomCap("Technique du Bucheron");
				c4.setNomCap("Attaque de Nain");
				c5.setNomCap("Lanc� de Hache");
				c6.setNomCap("Toupie Sanglante");
			}
			else if(this.nom=="Bout de bois")
			{
				c1.setNomCap("Pichenette");
				c2.setNomCap("Coup de Batte");
				c3.setNomCap("Charpente Extreme");
				c4.setNomCap("Technique du Menuisier");
				c5.setNomCap("Coup aux Testicules");
				c6.setNomCap("Lanc� de Poutre");
			}
			else if(this.nom=="Ep�e des rois")
			{
				c1.setNomCap("Estoc");
				c2.setNomCap("Taille");
				c3.setNomCap("Tourbillon");
				c4.setNomCap("Tr�panation");
				c5.setNomCap("Eventrement");
				c6.setNomCap("D�capitation");
			}
		}
		this.arc.add(c1);
		this.arc.add(c2);
		this.arc.add(c3);
		this.arc.add(c4);
		this.arc.add(c5);
		this.arc.add(c6);
	}
	
	public float getPBA(int statPba)//Mage==Intelligence || B & C == Dexterite
	{
		float pba = 0;
		if(this.role=='c' || this.role=='b')
		{
			pba=(statPba*this.getFAC())/1000;
			//System.out.println("Pba : statPba("+statPba+") * fac("+this.getFAC()+")/1000 ="+pba);
		}
		else if(this.role=='m')
		{
			pba=(statPba*this.getFAC())/1000;
			//System.out.println("Pba : statPba("+statPba+") * fac("+this.getFAC()+")/1000 ="+pba);
		}
		return pba;
	}
	
	public float getEFF(int statEff)//Mage==Concentration || B & C == Force
	{
		float eff = 0;
		if(this.role=='c' || this.role=='b')
		{
			eff=(statEff*this.getPUI())/100;
			//System.out.println("Eff : statEff("+statEff+") * pui("+this.getPUI()+")/100 ="+eff);
		}
		else if(this.role=='m')
		{
			eff=(statEff*this.getPUI())/100;
			//System.out.println("Eff : statEff("+statEff+") * pui("+this.getPUI()+")/100 ="+eff);
		}
		return eff;
	}
	
	public void afficheCapacite()
	{
		for(int i=0;i<arc.size();i++)
		{
			System.out.println(i+1 +" - "+ arc.get(i).toString());
		}
	}
	
	public String toString()
	{
		return  this.getNom() + "(Puissance : " + this.getPUI() + " || Facilit� : " + this.getFAC() +")";
	}
}
