
public class Capacite 
{
    private String nomCap;
    private int degats;
    private int soins;
    private int blocage;
    private double chance;
    
    public String getNomCap() {
		return nomCap;
	}
    public double getChance()
    {
    	return this.chance;
    }
    public void setChance(float chance)
    {
    	this.chance=chance;
    }

	public void setNomCap(String nomCap) {
		this.nomCap = nomCap;
	}

	public int getDegats() {
		return degats;
	}

	public void setDegats(int degats) {
		this.degats = degats;
	}

	public int getSoins() {
		return soins;
	}

	public void setSoins(int soins) {
		this.soins = soins;
	}

	public int getBlocage() {
		return blocage;
	}

	public void setBlocage(int blocage) {
		this.blocage = blocage;
	}

	public Capacite()
    {
    	
    }
    
    public Capacite(String nom, int d, int s, int b,double chance)
    {
    	this.nomCap=nom;
    	this.degats=d;
    	this.soins=s;
    	this.blocage=b;
    	this.chance=chance;
    }

	public String toString() 
	{
		return  nomCap + " (Degats:" + degats + "|Soins=" + soins + "|Blocage=" + blocage + "|Chance=" +chance + "]";
	}
    
}
