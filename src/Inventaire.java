
public class Inventaire 
{
	private Arme a;
	private Defense d;
	private Soin s;
	
	public Inventaire()
	{
		
	}
	
	public Inventaire(Arme a, Defense d, Soin s)
	{
		this.a=a;
		this.d=d;
		this.s=s;
	}
	
	public void iniInventaire(char c)
	{
		this.s=new Soin();
		this.s.creerSoin(c);
		this.a=new Arme();
		this.a.creerArme(c);
		this.d=new Defense();
		this.d.creerDefense(c);
	}
	
	public Arme getA() {
		return a;
	}

	public void setA(Arme a) {
		this.a = a;
	}

	public Defense getD() {
		return d;
	}

	public void setD(Defense d) {
		this.d = d;
	}

	public Soin getS() {
		return s;
	}

	public void setS(Soin s) {
		this.s = s;
	}

	public String toString()
	{
		return "Arme : " + this.a + "\nDefense : " + this.d + "\nSoin : " +this.s; 
	}

	public void afficheArme() 
	{
		this.a.toString();
		this.a.afficheCapacite();
	}

	public void afficheDefense() 
	{
		this.d.toString();
	}
	
	public void afficheSoin()
	{
		this.s.toString();
	}
}
