import java.util.Random;

public class Soin 
{
	private int REST;
	private int CH;
	private char role;
	private String nom;
	
	public Soin()
	{
		
	}
	public Soin(int REST,int CH,char role,String nom)
	{
		this.REST=REST;
		this.CH=CH;
		this.role=role;
		this.nom=nom;
	}
	public int getREST() {
		return this.REST;
	}
	public void setREST(int rEST) {
		this.REST = rEST;
	}
	public int getCH() {
		return this.CH;
	}
	public void setCH(int cH) {
		this.CH = cH;
	}
	public char getRole() {
		return this.role;
	}
	public void setRole(char role) {
		this.role = role;
	}
	public String getNom() {
		return this.nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public void creerSoin(char role)
	{
		Random random0 = new Random();
		int random = random0.nextInt(3);
		if(role=='c')
		{
			if(random==0)
			{
				this.setREST(40);
				this.setCH(60);
				this.setNom("Rem�de pr�par�");
			}
			else if(random==1)
			{
				this.setREST(80);
				this.setCH(20);
				this.setNom("Feuille de la compt�e");
			}
			else if(random==2)
			{
				this.setREST(20);
				this.setCH(80);
				this.setNom("Herbes de Provence");
			}
		}
		else if(role=='m')
		{
			if(random==0)
			{
				this.setREST(50);
				this.setCH(50);
				this.setNom("R�g�n�ration");
			}
			else if(random==1)
			{
				this.setREST(80);
				this.setCH(20);
				this.setNom("Repos Ultime");
			}
			else if(random==2)
			{
				this.setREST(20);
				this.setCH(80);
				this.setNom("Soins Rapide");
			}
		}
		else if(role=='b')
		{
			if(random==0)
			{
				this.setREST(40);
				this.setCH(60);
				this.setNom("Bandage des plaies");
			}
			else if(random==1)
			{
				this.setREST(80);
				this.setCH(20);
				this.setNom("Fil de fer");
			}
			else if(random==2)
			{
				this.setREST(20);
				this.setCH(80);
				this.setNom("Couteau Suisse");
			}
		}
		System.out.println("G�n�ration de Soin [" + this.getNom() + "] ( Restauration : " + this.getREST() + " || Chance : " + this.getCH() + " )");
	}
	
	/* Barbare
    Cat�gorie : soin
    Caract�ristiques : puissance PUI et facilit� FAC
    Probabilit� de r�ussite : PBA = DEX x FAC / 10000
    Efficacit� : EFF = FOR x PUI / 100 (la force est aussi un indicateur de la constiyution du personnage, lui permettant de r�cup�rer plus ou moins vite)
	 */
	
	/*
	 * MAGE & CHASSEUR
    Cat�gorie : soin
    Caract�ristiques : puissance PUI et facilit� FAC
    Probabilit� de r�ussite : PBA = INT x FAC / 10000
    Efficacit� : EFF = CON x PUI / 100
	 *
	 */
	
	public float getPBA(int statPba)
	{
		float pba=0;
		pba=(statPba*this.CH)/10;
		//System.out.println("Pba : statPba("+statPba+") * Chance("+this.getCH()+")/100 ="+pba);
		if(pba>100)
		{
			pba=100;
		}
		return pba;
	}
	
	public float getEFF(int statEff)
	{
		float eff=0;
		eff=(statEff*this.getREST())/1000;
		//System.out.println("Eff : stateff("+statEff+") * Restitution("+this.getREST()+")/1000 ="+eff);
		return eff;
	}

	public String toString() 
	{
		return this.getNom() + "(Restitution : " + this.getREST() + " || Chance : " + this.getCH() + ")";
	}
}
